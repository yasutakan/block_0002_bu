﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSelector : MonoBehaviour {
	public Camera FirstPersonCamera;
    public Camera MainCamera;
	public void ShowMainCamera() {
        FirstPersonCamera.enabled = false;
        MainCamera.enabled = true;
    }
    
    public void ShowFirstPersonCamera() {
        FirstPersonCamera.enabled = true;
        MainCamera.enabled = false;
    }

	Camera GetActiveCamera() {
    	if (FirstPersonCamera.enabled)
    		return FirstPersonCamera;
    	else
    		return MainCamera;
	}
	// Use this for initialization
	void Start () {
		//FirstPersonCamera = GameObject.FindWithTag("FPScameraTag");
		//MainCamera = GameObject.FindWithTag("MainCamera");
		ShowMainCamera();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("1")){
			ShowMainCamera();
		}
		if (Input.GetKeyDown("2")){
			ShowFirstPersonCamera();
		}
	}
}
