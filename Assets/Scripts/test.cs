using System;
using UnityEngine;
using System.Collections;

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Data<T> data = UnityEngine.JsonUtility.FromJson<Data<T>>(json);
        return data.jsonItems;
    }

    [Serializable]
    private class Data<T>
    {
        public T[] jsonItems;
    }
}
