﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace DataConfig{
public class SaveData_YN : MonoBehaviour{
	
	//Jsonクラスの定義
	[Serializable]
	public class BlockInfo
	{
		public string type;
		public int x;
		public int y;
		public int z;
	}

	[Serializable]
	public class Data
	{
		public List<BlockInfo> data = new List<BlockInfo>();
	}

	[SerializeField]
  	private static string _jsonText = "";

	//ゲームオブジェクト
	GameObject[] gameObject;
	// ブロックの種類
	public GameObject BlockObj;
	//Tagの配列 Blokの種類分だけ作る必要がある 実際にはどこか他の場所から取得してくる方が楽か
	string[] tag = {"BlockTag_Default"};
			
	//シーンにあるブロックを検索しSave
	public void Save(){
		//Dataクラスのインスタンス
		Data classData = new Data();

		for(int i = 0; i < tag.Length; i++){
			GameObject[] item = GameObject.FindGameObjectsWithTag(tag[i]);
			
				//デバッグ
				foreach(GameObject log in item){
					Debug.Log(log);
				}
			
			//string[] objName = new string[3];
			//Tagごとのアイテム数分の名前をスプリット
			for(int j = 0; j < item.Length; j++){
				string nameObj = item[j].transform.name;
				Debug.Log(nameObj);

				string[] objName = nameObj.Split('_');
				string nameType = objName[0];
				string nameX = objName[1];
				string nameY = objName[2];
				string nameZ = objName[3];

					//デバッグ
					Debug.Log(nameType);
					Debug.Log(nameX);
					Debug.Log(nameY);
					Debug.Log(nameZ);

				//XYZの数値部分のみを取得(Stringのまま)
				string posX = nameX.Substring(1);
				string posY = nameY.Substring(1);
				string posZ = nameZ.Substring(1);
				
					//デバッグ
					Debug.Log(posX);
					Debug.Log(posY);
					Debug.Log(posZ);

				//ブロックに対応するJsonを生成
				string jsonTmp = "{\"type\":\"" + nameType + "\",\"x\":\"" + posX + "\",\"y\":\"" + posY + "\",\"z\":\"" + posZ + "\"}";

					Debug.Log(jsonTmp);
				
				//Jsonテキストからクラスに変換
				BlockInfo jsonItems = JsonUtility.FromJson<BlockInfo>(jsonTmp);
					Debug.Log(jsonItems);

					//デバッグ
					//BlockInfo blockInfo = new BlockInfo();
					Debug.Log("item id " + jsonItems.type);
					Debug.Log("item name " + jsonItems.x);
					Debug.Log("item name " + jsonItems.y);
					Debug.Log("item name " + jsonItems.z);
				
				//JsonからつくったBlockInfoクラスをDataリストの末尾へ追加する
				classData.data.Add(jsonItems);
					Debug.Log(classData.data[0]);
			}
				
		}
		//クラスをJsonへ変換
		string serialisedItemJson = JsonUtility.ToJson(classData);
			Debug.Log("serialisedItemJson" + serialisedItemJson);

		//ファイルへ保存
		File.WriteAllText (GetSaveFilePath(), serialisedItemJson);
			//デバッグ
			foreach(var log in classData.data){
				Debug.Log(log);
			}
	}
	//=================================================================================
	//保存先のパス
	//=================================================================================
	//保存する場所のパスを取得。
	public static string GetSaveFilePath(){

	string filePath = "SaveData";

	//確認しやすいようにエディタではAssetsと同じ階層に保存し、それ以外ではApplication.persistentDataPath以下に保存するように。
	#if UNITY_EDITOR
	filePath += ".json";
	#else
	filePath = Application.persistentDataPath + "/" + filePath;
	#endif

	return filePath;
	
	}
	//JSONデータからデータを読み込み・展開
	public void ImportDataClass(){
		//Dataクラスのインスタンス
		Data deserialisedJsonItems = new Data();
		//Blockクラスのインスタンス
		//BlockInfo blockInfo = new BlockInfo();
		//JSONデータを読み込み
		deserialisedJsonItems = JsonUtility.FromJson<Data>(GetJson());
		
		for(int i = 0; i < deserialisedJsonItems.data.Count; i++ ){
			Debug.Log(deserialisedJsonItems.data[i]);
				//デバッグ
				Debug.Log("item id " + deserialisedJsonItems.data[i].type);
				Debug.Log("item id " + deserialisedJsonItems.data[i].x);
				Debug.Log("item id " + deserialisedJsonItems.data[i].y);
				Debug.Log("item id " + deserialisedJsonItems.data[i].z);

			//ブロックの情報を取得
			string nameType = deserialisedJsonItems.data[i].type;
			int posX = deserialisedJsonItems.data[i].x;
			int posY = deserialisedJsonItems.data[i].y;
			int posZ = deserialisedJsonItems.data[i].z;

			// ブロックを生成
			// 該当ブロックを選択する
			BlockObj = (GameObject)Resources.Load ("Prefab/BlockDefault_");
			//座標
			Vector3 getPos = new Vector3(posX - 0.5f ,posY - 0.5f ,posZ - 0.5f);
			// プレハブからインスタンスを生成
			GameObject Cube = Instantiate (BlockObj, getPos, Quaternion.identity);	
			//名前をつける
			// 型変換
			string xValueString = (posX).ToString();
			string yValueString = (posY).ToString();
			string zValueString = (posZ).ToString();
			//名前(座標)をつける
			Cube.name = BlockObj.name + "X" + xValueString + "_Y" + yValueString + "_Z" + zValueString;
				Debug.Log(BlockObj.name);
				Debug.Log(Cube.name);
			//string tmpJson = JsonUtility.ToJson(deserialisedJsonItems.data[i]);
			//Debug.Log(tmpJson);
			
		}

	}
	 private static string GetJson(){
    	//既にJsonを取得している場合はそれを返す。
		if(!string.IsNullOrEmpty(_jsonText)){
			return _jsonText;
			}

			//Jsonを保存している場所のパスを取得。
			string filePath = GetSaveFilePath();

			//Jsonが存在するか調べてから取得し変換する。存在しなければ新たなクラスを作成し、それをJsonに変換する。
			if(File.Exists(filePath)){
			_jsonText = File.ReadAllText (filePath);
			}
			else{
			_jsonText = JsonUtility.ToJson(new SaveData ());
			}

			return _jsonText;
  	}
}
}