﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour {
	//Ground Object
	public GameObject GroundObj;
	//Groundの広さ(ここは本来 UI の入力から取得)
	public int GroundX = 5;
	public int GroundZ = 5;
	//座標
	private float tmpX;
	private float tmpY;
	private float tmpZ;
	// Use this for initialization
	void GroundGenerating (int GroundX ,int GroundZ) {
		for (int j=1 ;j<=GroundZ ;j++ ){
			for (int i=1 ;i<=GroundX ;i++ ){
				//型変換 int -> float は自動でOK？
				GameObject Cube = Instantiate (GroundObj) as GameObject;
				Cube.transform.position = new Vector3 (i -0.5f, -0.5f, j -0.5f);
				//名前をつける
				//名前(座標)をつける
				Cube.name = GroundObj.name + "X" + i + "_Y0" + "_Z" + j;
				Debug.Log(Cube.name);
			}
		}
	}
	void Start () {
		//GroundObjectの取得(ここは本来 UI の入力から取得)※シーンにPrefabの実体があると、SaveData_YNが正常に働かないためここはコメントアウト
		//実際にはここで取得しなくても、Pubric変数にインスペクタ上でPrefabの実体を入れておけば問題ない
		//GroundObj = GameObject.FindWithTag("GroundTag");
		//Debug.Log(GroundObj);
		
		//Groundを生成
		GroundGenerating (GroundX,GroundZ);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
