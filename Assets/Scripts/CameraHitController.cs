﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yn_camera
{
	public class CameraHitController : MonoBehaviour {
		// 座標ベクトル
		private Vector3 getPos;
		// 法線ベクトル
		private Vector3 getNormal;
		// 名前
		private string objName;
		// Raycasterによって示されるオブジェクト
		private string pointedObjTag;
		// ブロックの種類
		public GameObject BlockObj;
		//座標
		private float tmpX;
		private float tmpY;
		private float tmpZ;
		//クリック(タップ)認識中のフラグ
		private bool isDoubleTapStart;
		//クリック（タップ）開始からの累積時間
		private float doubleTapTime;

		//CameraSelector（後々、別のクラスとしたい）
			//FPS camera
			public Camera FirstPersonCamera;
			//Main Camera
			public Camera MainCamera;
			//FPS カメラに関連するGameObject
			public GameObject FpsCameraGameObj;
			//MAIN カメラに関連するGameObject
			public GameObject MainCameraGameObj;
		//MAINカメラをアクティブにして、FPSカメラとそれに関連するGameObjectを非アクティブとする
		public void ShowMainCamera() {
			FirstPersonCamera.enabled = false;
			MainCamera.enabled = true;
			
			FpsCameraGameObj.SetActive(false);
			MainCameraGameObj.SetActive(true);
		}
		//FPSカメラにをアクティブにして、関連するGameObjectを非アクティブとする
		public void ShowFirstPersonCamera() {
			FirstPersonCamera.enabled = true;
			MainCamera.enabled = false;

			MainCameraGameObj.SetActive(false);
			FpsCameraGameObj.SetActive(true);
		}

		public Camera GetActiveCamera() {
			if (FirstPersonCamera.enabled)
				return FirstPersonCamera;
			else
				return MainCamera;
		}
		
		// Use this for initialization
		void Start () {
			// BlockObjectの取得(初期化)
			BlockObj = GameObject.FindWithTag("BlockTag_Default");
			//FPS カメラの取得
			GameObject FpsCameraGameObj = GameObject.Find("FirstPersonCharacter");
				Debug.Log(FpsCameraGameObj);
			//MAIN カメラの取得
			GameObject MainCameraGameObj = GameObject.Find("Main Camera");
				Debug.Log(MainCameraGameObj);
			//最初のカメラはMain Cameraとする
			ShowMainCamera();
		}
		
		// Update is called once per frame
		void Update () {
			// BlockObjectの種類(本当はここで今選択しているBlockObjectをUIの変数より選ぶ)FindWithTagではシーンにあるオブジェクトしか取得できない
			// BlockObj = GameObject.FindWithTag("BlockTag_Default");
			// プレハブを取得
			
			// Raycast変数の宣言
			RaycastHit hit;
			Ray ray;
			
			// 画面をクリック（タップ）した時
			if (Input.GetMouseButtonDown(0)) {	
				ray = GetActiveCamera().ScreenPointToRay (Input.mousePosition);
				Debug.Log(ray);
				
				if (Physics.Raycast(ray ,out hit ,Mathf.Infinity)) {
					// オブジェクトの座標を取得（LOGはひとつのオブジェクトに対して一定でないのでクリックしたところの座標？？）
					getPos = hit.point;
						//それぞれの座標は以下のように記述することで、精度が確認できる
						Debug.Log(getPos.x);
						Debug.Log(getPos.y);
						Debug.Log(getPos.z);
					// Normalを取得
					getNormal = hit.normal;
						Debug.Log(getNormal);
						Debug.DrawRay(hit.point, getNormal, Color.green);
					// objectの名前を取得
					objName = hit.collider.gameObject.name;
						Debug.Log(objName);
					// objectの座標を取得
					Vector3 tmp = hit.collider.gameObject.transform.position;
						Debug.Log(tmp);
					
					pointedObjTag = hit.transform.gameObject.tag;
						Debug.Log(pointedObjTag);
					
					//ブロックを作成
					this.CreateBlock(BlockObj,getPos,getNormal,name,tmp);

				}
			}

			// 画面をダブルクリック（ダブルタップ）した時ブロックを削除する（取る）ことを実装する（WIP）
			if (Input.GetMouseButtonDown(0)) {
				ray = GetActiveCamera().ScreenPointToRay (Input.mousePosition);
				Debug.Log(ray);
				//https://qiita.com/morio36/items/3de164d6131c902af9bd を参考に実装
			}

			//ブロックのY軸回転（未実装）

			//CameraSelector（後々、別のクラスとしたい）
			if (Input.GetKeyDown(KeyCode.Alpha1)){
				ShowMainCamera();
			}
			if (Input.GetKeyDown(KeyCode.Alpha2)){
				ShowFirstPersonCamera();
			}
		}
		//ブロックを作成する関数（Rayが当たったブロックに応じた動作）
		void CreateBlock(GameObject BlokObj, Vector3 getPos, Vector3 getNormal, string objName, Vector3 tmp){
			
			// プロジェクトの Prefabからブロックを選択する（本来はUIから選択したものを選ぶ）
			BlockObj = (GameObject)Resources.Load ("Prefab/BlockDefault_");
			// プレハブからインスタンスを生成
			GameObject Cube = Instantiate (BlockObj, getPos, Quaternion.identity);	
			// BlockをPrefabからつくりそれに対応した場所にCubeを移動(上記のスクリプトの方が新しい為、ここはコメントアウト)
			// GameObject Cube = Instantiate (BlockObj) as GameObject;

			//Groundをポイントした場合
			if(pointedObjTag == "GroundTag"){
				Cube.transform.position = new Vector3 (tmp.x ,tmp.y + 1.0f, tmp.z);
			}
			//Blockをポイントした場合
			if(pointedObjTag != "GroundTag"){
				Cube.transform.position = new Vector3 (tmp.x + getNormal.x ,tmp.y + getNormal.y, tmp.z + getNormal.z);
				//制限以上の座標のCubeは削除することを実装する（未実装）
			}
			//Terrainをポイントした場合、Blockを削除
			if(pointedObjTag == "TerrainTag"){
				Destroy(Cube);
			}

			// 名前付けのためのpositionから位置付け
			float xValue = Cube.transform.position.x + 0.5f;
			float yValue = Cube.transform.position.y + 0.5f;
			float zValue = Cube.transform.position.z + 0.5f;
				Debug.Log(xValue);
				Debug.Log(yValue);
				Debug.Log(zValue);
			// 型変換
			string xValueString = (xValue).ToString();
			string yValueString = (yValue).ToString();
			string zValueString = (zValue).ToString();
			//名前(座標)をつける
			Cube.name = BlockObj.name + "X" + xValueString + "_Y" + yValueString + "_Z" + zValueString;
				Debug.Log(BlockObj.name);
				Debug.Log(Cube.name);
		}
		//座標をJSON変換用のクラスにして、GameObjectにアタッチする関数
		//
	}
}
